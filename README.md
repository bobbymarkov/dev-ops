# Introduction

The "DevOps" project automates the deployment process upon merging to the main branch in GitLab, utilizing a CI/CD pipeline. It leverages Terraform state management to oversee an AWS infrastructure, where initial Terraform runs establish an autoscale group with a load balancer for backend instances, a S3 bucket for frontend assets, and an RDS database. Subsequent pipeline stages involve building the frontend using Flutter, conducting backend testing with Gradle, and creating Docker images for frontend, backend, and database components, uploaded to GitLab's Container Registry. Through SSH, the CI/CD pipeline accesses running instances to pull the latest backend image from the Container Registry, enabling seamless updates. Built frontend web files are then uploaded to the S3 bucket, culminating in a static website hosted on S3. This website is seamlessly integrated with a load balancer, distributing requests to EC2 instances accessing the RDS database for data retrieval.

# Project information

- AWS must be running.
- Generate a personal access token in gitlab.
- Change the following Gitlab CI/CD variables:
  - P_ACCESS_KEY - Assign the personal access token.
  - LOGIN_USERNAME 
  - LOGIN_PASSWORD 
  - MY_AWS_ACCESS_KEY_ID
  - MY_AWS_SECRET_ACCESS_KEY
  - MY_AWS_SESSION_TOKEN
- If you want you can also change data inside the ./infra/variables.tf file.

If it is set up correctly, a push to main will trigger the entire system. No other manual input is required. 
The front end is a static website uploaded to a S3 bucket. 
The database is set up to use RDS. 
The back end uses a load balancer and an auto-scale group.

The docker images are built within the CI/CD, they are saved into the gitlab registry, and then the AWS EC2 instances pull the image.

Currently, the S3 bucket uses a randomly generated name, to connect to it you can find it in the Gitlab CI/CD variable BUCKET_DNS.

Made with love - Borislav and Viktor